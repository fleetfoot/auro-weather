import request from "superagent";

class LocationService {
  constructor(search) {
    this.locationSearchTypeAhead(search);
    this.queriedLocations = [];
  }

  locationSearchTypeAhead(search) {
    const url = `https://secure.geonames.org/searchJSON?name_startsWith=${search}&countryBias=NO&featureClass=P&maxRows=5&isNameRequired=true&style=FULL&username=${
      process.env.VUE_APP_GEONAMES_USERNAME
    }`;

    request
      .get(url)
      .then(jsonLocations => this.getLocations(jsonLocations.body));
  }

  getLocations(jsonLocations) {
    let municipality = "";
    let placeName = "";
    let countyName = "";
    let latitude = "";
    let longitude = "";
    let timeZone = "";

    for (let i = 0; i < jsonLocations.geonames.length; i++) {
      placeName = jsonLocations.geonames[i].name;
      municipality = jsonLocations.geonames[i].adminName1;
      countyName = jsonLocations.geonames[i].countryName;
      latitude = jsonLocations.geonames[i].lat;
      longitude = jsonLocations.geonames[i].lng;
      timeZone = jsonLocations.geonames[i].timezone.timeZoneId;

      this.populateLocationTypeAhead(
        placeName,
        municipality,
        countyName,
        latitude,
        longitude,
        timeZone
      );
    }
  }

  populateLocationTypeAhead(
    placeName,
    municipality,
    countyName,
    latitude,
    longitude,
    timeZone
  ) {
    this.queriedLocations.push({
      placeName: placeName,
      municipality: municipality,
      countyName: countyName,
      latitude: latitude,
      longitude: longitude,
      timeZone: timeZone
    });
  }
}

export default LocationService;
