class WeatherService {
  constructor(lat, long) {
    this.weatherData = [];
    this.fetchLocationWeather(lat, long);
  }

  fetchLocationWeather(lat, long) {
    const url = `https://api.met.no/weatherapi/locationforecast/1.9/?lat=${lat}&lon=${long}`;

    return fetch(url)
      .then(response => response.text())
      .then(string => {
        return new window.DOMParser().parseFromString(string, "text/xml");
      })
      .then(xmlWeather => this.getWeather(xmlWeather));
  }

  getWeather(xmlWeather) {
    let date = "";
    let lowClouds = 0;
    let mediumClouds = 0;
    let highClouds = 0;
    for (let i = 0; i < 48; i++) {
      date = xmlWeather.getElementsByTagName("time")[i * 5].getAttribute("to");
      lowClouds = xmlWeather
        .getElementsByTagName("lowClouds")
        [i].getAttribute("percent");
      mediumClouds = xmlWeather
        .getElementsByTagName("mediumClouds")
        [i].getAttribute("percent");
      highClouds = xmlWeather
        .getElementsByTagName("highClouds")
        [i].getAttribute("percent");

      this.populateWeaterData(date, lowClouds, mediumClouds, highClouds);
    }
  }
  populateWeaterData(hour, lowClouds, mediumClouds, highClouds) {
    this.weatherData.push({
      hour: hour,
      lowClouds: lowClouds,
      mediumClouds: mediumClouds,
      highClouds: highClouds
    });
  }
}

export default WeatherService;
