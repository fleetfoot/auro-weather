import request from "superagent";

class NorwegianLocationService {
  constructor(search) {
    this.locationSearchTypeAheadNorway(search);
    this.queriedLocations = [];
  }

  locationSearchTypeAheadNorway(search) {
    const url = `https://ws.geonorge.no/SKWS3Index/ssr/sok?navn=${search}*&antPerSide=5&epsgKode=4258`;

    request
      .get(url)
      .then(blob => blob.text)
      // .catch(error => error.message)
      .then(str => new DOMParser().parseFromString(str, "text/xml"))
      .then(locations => this.getLocations(locations));
  }

  getLocations(xmlLocations) {
    let municipality = "";
    let placeName = "";
    let nameType = "";
    let countyName = "";

    for (let i = 0; i < 6; i++) {
      placeName = xmlLocations.getElementsByTagName("skrivemaatenavn")[i]
        .textContent;
      nameType = xmlLocations.getElementsByTagName("navnetype")[i].textContent;
      municipality = xmlLocations.getElementsByTagName("kommunenavn")[i]
        .textContent;
      countyName = xmlLocations.getElementsByTagName("fylkesnavn")[i]
        .textContent;

      this.populateLocationTypeAhead(
        placeName,
        nameType,
        municipality,
        countyName
      );
    }
  }

  populateLocationTypeAhead(placeName, nameType, municipality, countyName) {
    this.queriedLocations.push({
      placeName: placeName,
      nameType: nameType,
      municipality: municipality,
      countyName: countyName
    });
  }
}

export default NorwegianLocationService;
