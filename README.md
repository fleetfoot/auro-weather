# Hi Clouds

This project was created to pull weather data from Yr.no specifically for the use of easily getting important cloud information for finding the northern lights. But of course in can be used for any use case when low, medium and high clouds are needed.

I also used this project to practice making API calls and practice using [Vue.js](https://www.vuejs.org)

## Technologies Used

- Vue.js
- JavaScript (ES6)
- Webpack
- moment.js

## APIs

- [MET Norway](https://api.met.no/)
- [GeoNames](http://www.geonames.org/)

## TODO

- [ ] Loading gif while content is loaded
- [ ] Store recent searches in a cookie so users can come back to the same state they left it at
- [x] Add Dates above new day in Table
- [x] Recent Location Buttons

## Bugs

- [ ] TypeAhead not opening on mobile when soft keyboard is open. This is an issue in Vue discussed [here](https://github.com/vuejs/vue/issues/8231). I have tried the workarounds but they did not work for me.

## Project setup

Create a file in root called

```
.env.local
```

and add the the following line replacing `yourusername` with the username you register at [Geonames](http://www.geonames.org/login)

```
VUE_APP_GEONAMES_USERNAME="yourusername"
```

then

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
